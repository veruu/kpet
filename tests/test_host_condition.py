# Copyright (c) 2021 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Test cases for the host_condition module"""
import unittest

from kpet.host_condition import from_factored_minterms
from kpet.host_condition import minimize


class HostConditionTest(unittest.TestCase):
    """Test cases for the host_condition module."""

    def test_from_factored_minterms(self):
        """Check from_factored_minterms() works correctly"""
        def ffm(minterms):
            return from_factored_minterms(["T:A", "T:B", "T:C"],
                                          minterms)
        # No terms
        self.assertEqual([], ffm([]))
        # An empty term
        self.assertEqual([], ffm([(0, 7)]))
        # Two empty terms
        self.assertEqual([], ffm([(0, 7), (0, 7)]))
        # Single one-variable term
        self.assertEqual('T:A', ffm([(1, 6)]))
        # Single two-variable term
        self.assertEqual(['T:A', 'T:B'], ffm([(3, 4)]))
        # Single three-variable term
        self.assertEqual(['T:A', 'T:B', 'T:C'], ffm([(7, 0)]))
        # Double one-variable term
        self.assertEqual('T:A', ffm([(1, 6), (1, 6)]))
        # Double two-variable term
        self.assertEqual(['T:A', 'T:B'], ffm([(3, 4), (3, 4)]))
        # Double three-variable term
        self.assertEqual(['T:A', 'T:B', 'T:C'], ffm([(7, 0), (7, 0)]))
        # Redundant term
        self.assertEqual(['T:A', 'T:B'], ffm([(3, 4), (1, 6)]))
        # Shared term
        self.assertEqual(['T:B', {'or': ['T:A', 'T:C']}],
                         ffm([(3, 4), (6, 1)]))

    def test_minimize(self):
        """Check minimize() works correctly"""
        self.assertEqual([], minimize([]))
        self.assertEqual("H:A", minimize("H:A"))
        self.assertEqual("T:A", minimize("T:A"))
        self.assertEqual("H:A", minimize(["H:A"]))
        self.assertEqual("H:A", minimize([["H:A"]]))
        self.assertEqual("H:A", minimize(["H:A", "H:A"]))
        self.assertEqual("H:A", minimize({"or": ["H:A", "H:A"]}))
        self.assertEqual({"not": "H:A"}, minimize({"not": "H:A"}))
        self.assertEqual("H:", minimize([{"not": "H:A"}, "H:A"]))
        self.assertEqual([], minimize({"or": [{"not": "H:A"}, "H:A"]}))
        self.assertEqual(["H:A", "H:B"],
                         minimize(["H:A", "H:B"]))
        self.assertEqual({"or": ["H:A", "H:B"]},
                         minimize({"or": ["H:A", "H:B"]}))
        self.assertEqual({"or": ["H:A", "H:B"]},
                         minimize([{"or": ["H:A", "H:B"]},
                                   {"or": ["H:B", "H:A"]}]))
        self.assertEqual({'or': [[{'not': 'H:A'}, {'not': 'H:B'}],
                                 ['H:A', 'H:B']]},
                         minimize([{"or": [{"not": "H:A"}, "H:B"]},
                                   {"or": [{"not": "H:B"}, "H:A"]}]))
        self.assertEqual({'or': [[{'not': 'H:A'}, {'not': 'H:B'}],
                                 ['H:A', 'H:B']]},
                         minimize({'or': [[{'not': 'H:A'}, {'not': 'H:B'}],
                                          ['H:A', 'H:B']]}))

        self.assertEqual('H:A',
                         minimize({'or': [['H:A', 'H:B'], ['H:A']]}))
        self.assertEqual(['H:A', {'or': ['H:B', 'H:C']}],
                         minimize(['H:A', {'or': ['H:B', 'H:C']}]))

        self.assertEqual(['H:A', 'H:B', {'or': ['H:D', 'H:C']}],
                         minimize(['H:A', 'H:B', {'or': ['H:D', 'H:C']}]))

        self.assertEqual([{'not': 'H:A'}, {'or': ['H:B', 'H:C']}],
                         minimize([{'not': 'H:A'}, {'or': ['H:B', 'H:C']}]))

        self.assertEqual(['H:A',
                          {'or': [{'not': 'H:B'}, 'H:C']}],
                         minimize(['H:A',
                                  {'or': [{'not': 'H:B'}, 'H:C']}]))

        self.assertEqual([{'not': 'H:A'},
                          {'or': [{'not': 'H:B'}, 'H:C']}],
                         minimize([{'not': 'H:A'},
                                  {'or': [{'not': 'H:B'}, 'H:C']}]))

        self.assertEqual([{'not': 'H:A'},
                          {'or': [{'not': 'H:C'}, {'not': 'H:B'}]}],
                         minimize([{'not': 'H:A'},
                                  {'or': [{'not': 'H:C'}, {'not': 'H:B'}]}]))

        self.assertEqual(["H:A", "H:B", "H:C", "H:D", "H:E", "H:F",
                          "H:G", "H:H", "H:I", "H:J", "H:K", "H:L"],
                         minimize(["H:A", "H:B", "H:C", "H:D", "H:E", "H:F",
                                   "H:G", "H:H", "H:I", "H:J", "H:K", "H:L"]))
